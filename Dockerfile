FROM node:12.13.0-alpine
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
COPY yarn.lock ./
RUN apk --no-cache add curl
RUN yarn install
COPY . ./
EXPOSE 3000
CMD ["npm","start"]